# repl.it-color
 
Allows customisation of the [repl.it](https://repl.it) color scheme (syntax highlighting).

To install, drag `replit-color.crx` on to your `chrome://extensions` page. Change settings by clicking on the logo in the extensions bar and then clicking `options` in the drop down menu.